from single_winner import *
from collections import Counter
import math

################################################################################


def votes_needed_to_win(ballots, num_winners):
    '''
    >>> votes_needed_to_win([{'CPC':3,'NDP':5}, {'NDP':2,'CPC':4},{'CPC':3, 'NDP':5}], 1) # 22 total votes
    2
    >>> votes_needed_to_win(['g']*20, 2)
    7
    >>> g = ['GREEN1', 'GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1']
    >>> n = ['NDP1', 'NDP2', 'GREEN1', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3']
    >>> votes_needed_to_win([g]*2 + [n]*2, 4)
    1
    >>> g = ['GREEN1', 'GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1']
    >>> n = ['NDP1', 'NDP2', 'GREEN1', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3']
    >>> votes_needed_to_win([g]*5 + [n]*3, 4)
    2
    '''
    # each item in list "ballots" is considered one ballot, or one vote per the droop quota formula.
    num_ballots = 0

    for i in ballots:
        num_ballots = num_ballots + 1

    quota = int(num_ballots / (num_winners + 1)) + 1
    return quota

def has_votes_needed(result, votes_needed):
    '''
    >>> has_votes_needed({'NDP': 4, 'LIBERAL': 3}, 4)
    True
    >>> has_votes_needed({'NDP': 2, 'LIBERAL': 3}, 4)
    False
    >>> has_votes_needed({'NDP': 2, 'LIBERAL': 3}, 1)
    True
    >>> has_votes_needed({'NDP': 2, 'LIBERAL': 5}, 3)
    True
    '''
    for i in result.values():
        if i >= votes_needed:
            return True
    return False

################################################################################


def eliminate_candidate(ballots, to_eliminate):
    '''
    >>> eliminate_candidate([['NDP', 'LIBERAL'], ['GREEN', 'NDP'], ['NDP', 'BLOC']],['NDP', 'LIBERAL'])
    [[], ['GREEN'], ['BLOC']]
    >>> eliminate_candidate([['NDP', 'LIBERAL'], ['GREEN', 'NDP'], ['GREEN', 'BLOC']],['NDP', 'LIBERAL'])
    [[], ['GREEN'], ['GREEN', 'BLOC']]
    '''
    new_ballots = []
    # cycle through each ballot and remove ones we don't need, then make a new ballot with the ones we want only.
    for individual_ballot in ballots:
        new_individual_ballot = []
        for candidate in individual_ballot:
            if candidate not in to_eliminate:
                new_individual_ballot.append(candidate)

        new_ballots.append(new_individual_ballot)


    return new_ballots

################################################################################



def count_irv(ballots):
    '''
    >>> count_irv([['NDP'], ['GREEN', 'NDP', 'BLOC'], ['LIBERAL','NDP'],['LIBERAL'], ['NDP', 'GREEN'], ['BLOC', 'GREEN', 'NDP'],['BLOC', 'CPC'], ['LIBERAL', 'GREEN'], ['NDP']])
    {'BLOC': 0, 'CPC': 0, 'GREEN': 0, 'LIBERAL': 3, 'NDP': 5}
    '''

    flattened_ballots = flatten_lists(ballots)
    candidate_count = len(set(flattened_ballots)) # 5 for doctest 1
    parties_eliminated = {}
    for j in range(0, candidate_count):
        # make the data accessible for us by looking at the total ballots.
        eliminated_candidate = False
        parties_with_votes = count_first_choices(ballots)
        all_votes = []
        for i in parties_with_votes.values():
            all_votes.append(i)

        # determine if there is a winning majority or not
        potential_winning_votes = max(all_votes)
        if potential_winning_votes > int(math.ceil(sum(all_votes) / 2)):
            # sorts dictionary by values, from low to high
            unsorted = dict(Counter(add_dicts(parties_with_votes, parties_eliminated)))
            finished = {}
            for key in sorted(unsorted.keys()):
                finished[key] = unsorted[key]
            return finished
        else:
            what_to_look_at = min(all_votes)

            # if a party got 0 votes theres no need to actually "eliminate" it from our program's sense, as that wouldn't change the vote results
            # we wouldn't have gotten any closer to finding a winner as no ballots has a party as their first pick.
            if what_to_look_at == 0:
                organized_all_votes = sorted(all_votes)
                for i in organized_all_votes:
                    if i > 0:
                        what_to_look_at = i # look at party with second least votes instead of one with none
                        break

            index_to_eliminate = all_votes.index(what_to_look_at)
            party_to_eliminate = str(list(parties_with_votes.keys())[index_to_eliminate])
            for l in ballots:
                try:
                    if l[0] == party_to_eliminate:
                        l.remove(l[0])
                        parties_eliminated[party_to_eliminate] = 0 # we need this so we have "party: 0" in every case
                except IndexError:
                    pass
        '''
        # help us determine if there is elimination to be done
        occurrences_max = 0
        for i in range(0, len(all_votes) - 1):
            if max(all_votes) == all_votes[i]:
                occurrences_max = occurrences_max + 1
        print(all_votes)
        print('if this is 2 or higher elimination is required: ' + str(occurrences_max))
        # this means we have to eliminate the last place candidate
        if occurrences_max > 1:
        
            what_to_look_at = min(all_votes)
            # if a party got 0 votes theres no need to actually "eliminate" it from our program's sense, as that wouldn't change the vote results
            # we wouldn't have gotten any closer to finding a winner as no ballots has a party as their first pick.

            if what_to_look_at == 0:
                organized_all_votes = sorted(all_votes)
                print(organized_all_votes)
                print(all_votes)
                what_to_look_at = organized_all_votes[1]

            print('candidate to eliminate got ' + str(what_to_look_at) + ' vote')
            print(all_votes)
            index_to_eliminate = all_votes.index(what_to_look_at)
            print(index_to_eliminate)
            party_to_eliminate = str(list(parties_with_votes.keys())[index_to_eliminate])
            print('Party to eliminate: ' + party_to_eliminate)
            counter = 0
            for l in ballots:
                if l[0] == party_to_eliminate:
                    l.remove(l[0])
                    counter = counter + 1
                elif counter == 0:
                    print('found none to eliminate')
        else:
            print('Winner: ' + str(max(all_votes)))
            # sorts dictionary by values, from low to high
            return {k: v for k, v in sorted(parties_with_votes.items(), key=lambda item: item[1])} # requires python > 3.6

'''


################################################################################

if __name__ == '__main__':
    doctest.testmod()
