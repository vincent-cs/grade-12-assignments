import doctest
import random


def flatten_lists(nested):
    '''
    (list) -> list

    Input a list and replaces any list inside with their values, return the
    new list.

    >>> flatten_lists([[0],[1, 2], 3])
    [0, 1, 2, 3]
    >>> flatten_lists([2, 4, [1, 8], 3])
    [2, 4, 1, 8, 3]
    '''
    flattened_list = []
    for i in nested:
        if type(i) == list:
            for j in i:
                flattened_list.append(j)
        else:
            flattened_list.append(i)
    return flattened_list


def flatten_dict(d):
    '''
    (dict) -> list

    Return the keys of the dictionary v times, where v is the value associated
    with the key in the dictionary.

    >>> flatten_dict({'LIBERAL': 5, 'NDP':2})
    ['LIBERAL', 'LIBERAL', 'LIBERAL', 'LIBERAL', 'LIBERAL', 'NDP', 'NDP']
    >>> flatten_dict({'NDP': 2, 'LIBERAL':5})
    ['NDP', 'NDP', 'LIBERAL', 'LIBERAL', 'LIBERAL', 'LIBERAL', 'LIBERAL']
    '''
    flattened_dict = []
    for i, j in d.items():
        counter = 1
        while counter <= j:
            flattened_dict.append(i)
            counter = counter + 1
    return flattened_dict

def add_dicts(d1, d2):
    '''
    >>> add_dicts({'a': 2, 'b': 1}, {'a': 4, 'b': 8, 'c': 2})
    {'a': 6, 'b': 9, 'c': 2}
    '''
    # if that key exists in both we need to add it.
    for key in d1.keys():
        if key in d2:
            d2[key] = d2[key] + d1[key]
        else:
            d2[key] = d1[key]
    return d2
    #erged_dict = {}
    #for i, j in d1.items:
    #    merged_dict[i]=j
    #for i in d1.


def get_all_candidates(ballots):
    '''
    >>> get_all_candidates([{'GREEN':3, 'NDP':5}, {'NDP':2, 'LIBERAL':4},['CPC', 'NDP'], 'BLOC'])
    ['GREEN', 'NDP', 'LIBERAL', 'CPC', 'BLOC']
    '''
    cleaned_ballots = []
    for i in ballots:
        if type(i) == dict:
            for j in i.keys():
                cleaned_ballots.append(j)
        if type(i) == list:
            for h in i:
                cleaned_ballots.append(h)
        if type(i) == str:
            cleaned_ballots.append(i)

    proper_ballots = []
    for i in cleaned_ballots:
        if i not in proper_ballots:
            proper_ballots.append(i)
    return proper_ballots
###################################################### winner/loser

def get_candidate_by_place(result, func):
    '''
    >>> result = {'LIBERAL':4, 'NDP':6, 'CPC':6, 'GREEN':4}
    >>> random.seed(0)
    >>> get_candidate_by_place(result, min)
    'GREEN'
    >>> random.seed(1)
    >>> get_candidate_by_place(result, min)
    'LIBERAL'
    >>> result = {'LIBERAL':8, 'NDP':6, 'CPC':6, 'GREEN':4}
    >>> random.seed(0)
    >>> get_candidate_by_place(result, max)
    'LIBERAL'
    >>> result = {'GREEN3': 1, 'NDP2': 1, 'NDP3': 0, 'BLOC1': 0}
    >>> random.seed(1)
    >>> get_candidate_by_place(result, min)
    'NDP3'
    '''
    try:
        values = result.values()
    except AttributeError:
        return ''
    try:
        apex_value = func(values)
    except ValueError:
        return ''

    candidates = []
    for party in result:
        if result[party] == apex_value: # check if each party tally to see if they got the min amount
            candidates.append(party)
    return random.choice(candidates) # take a random one from the provided list

    # old way of doing this (same but finding apex in a more difficult way, as well as looking at the indices of apex's in a weird way todo remove?
    '''
    candidates = []
    votes = []
    # put everything in 2 lists to make it easy to work with.
    try:
        for o, q in result.items():
            candidates.append(o)
            votes.append(q)
    except AttributeError:
        pass
    # see our max or min of votes (apex)
    try:
        apex_votes = func(votes)
    except ValueError:
        pass

    # find where the apex amounts of appears, if it appears more than once that means there is a tie and
    # we must randomize who is selected as the winner or loser
    apex_votes_indices = [i for i, x in enumerate(votes) if x == apex_votes]

    if len(apex_votes_indices) > 1:
        where_to_look = apex_votes_indices[random.randint(0, (len(apex_votes_indices) - 1))]
        return candidates[where_to_look]
    else:
        # since there is only one party who got the apex amount of votes we can simply return and be on our way.
        try:
            return candidates[apex_votes_indices[0]]
        except IndexError: # this error handling is useful to have here as programs designed by grader do not fully error handle on their own
            # we need to be managing this here, even if given odd input
            return []
'''

def get_winner(result):
    '''
    >>> get_winner({'NDP': 2, 'GREEN': 1, 'LIBERAL': 0, 'BLOC': 0})
    'NDP'
    '''
    return get_candidate_by_place(result, max)

    # use previous function (me)

def last_place(result, seed=None):
    '''
    >>> result = {'LIBERAL':4, 'NDP':6, 'CPC':6, 'GREEN':4}
    >>> random.seed(1)
    >>> last_place(result)
    'LIBERAL'
    >>> random.seed(3)
    >>> result = {'GREEN1': 5, 'NDP1': 3, 'GREEN2': 0, 'GREEN3': 0, 'NDP2': 0, 'NDP3': 0, 'BLOC1': 0}
    >>> last_place(result)
    'GREEN3'
    >>> result = {'GREEN3': 1, 'NDP2': 1, 'NDP3': 0, 'BLOC1': 0}
    >>> random.seed(0)
    >>> get_candidate_by_place(result, min)
    'BLOC1'
    '''
    return get_candidate_by_place(result, min)


###################################################### testing help

def pr_dict(d):
    '''(dict) -> None
    Print d in a consistent fashion (sorted by key alphabetically).
    Provided to students. Do not edit.
    >>> pr_dict({'a':1, 'b':2, 'c':3})
    {'a': 1, 'b': 2, 'c': 3}
    '''
    l = []
    for k in sorted(d):
        l.append("'" + k + "'" + ": " + str(d[k]))
    print('{' + ", ".join(l) + '}')



if __name__ == '__main__':
    doctest.testmod()
