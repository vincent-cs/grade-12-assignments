from collections import Counter
from a3_helpers import *


def count_plurality(ballots):
    '''
    >>> ballots = ['LIBERAL', 'LIBERAL', 'NDP', 'LIBERAL']
    >>> count_plurality(ballots)
    {'LIBERAL': 3, 'NDP': 1}
    >>> ballots = ['GREEN1', 'GREEN1', 'GREEN1', 'GREEN1', 'GREEN1', 'NDP1', 'NDP1', 'NDP1']
    >>> count_plurality(ballots)
    {'GREEN1': 5, 'NDP1': 3}
    >>> ballots = ['LIBERAL', 'NDP', 'NDP', 'NDP']
    >>> count_plurality(ballots)
    {'NDP': 3, 'LIBERAL': 1}
    '''
    unsorted = dict(Counter(ballots))
    return {k: v for k, v in sorted(unsorted.items(), key=lambda item: item[1], reverse=True)}

def count_approval(ballots):
    '''
    >>> count_approval([['LIBERAL', 'NDP'], ['NDP'], ['NDP', 'GREEN', 'BLOC']] )
    {'LIBERAL': 1, 'NDP': 3, 'GREEN': 1, 'BLOC': 1}
    '''
    return dict(Counter(flatten_lists(ballots)))

def count_rated(ballots):
    '''
    >>> count_rated([{'LIBERAL': 5, 'NDP':2}, {'NDP':4, 'GREEN':5}])
    {'LIBERAL': 5, 'NDP': 6, 'GREEN': 5}
    >>> count_rated([{'CPC': 3, 'NDP': 5}, {'NDP': 2, 'CPC': 4}, {'CPC': 3, 'NDP': 5}])
    {'CPC': 10, 'NDP': 12}
    >>> count_rated([{'g': 20}])
    {'g': 20}
    >>> count_rated([['GREEN1', 'GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1'], ['GREEN1', 'GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1'], ['NDP1', 'NDP2', 'GREEN1', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3'], ['NDP1', 'NDP2', 'GREEN1', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3']])
    {'GREEN1': 4, 'GREEN2': 4, 'GREEN3': 2, 'NDP1': 4, 'NDP2': 4, 'NDP3': 6, 'BLOC1': 4}
    '''
    vote_count = []
    for i in ballots:
        if type(i) == dict:
            flattened_dict = flatten_dict(i)
            vote_count = vote_count + flattened_dict
        if type(i) == list:
            flattened_dict = flatten_lists(i)
            vote_count = vote_count + flattened_dict
    return dict(Counter(vote_count))

def count_first_choices(ballots):
    '''
    >>> count_first_choices([['NDP', 'LIBERAL'], ['GREEN', 'NDP'], ['NDP', 'BLOC']])
    {'NDP': 2, 'GREEN': 1, 'LIBERAL': 0, 'BLOC': 0}
    >>> g = ['GREEN1', 'GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1']
    >>> n = ['NDP1', 'NDP2', 'GREEN1', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3']
    >>> count_first_choices([g]*5 + [n]*3)
    {'GREEN1': 5, 'NDP1': 3, 'GREEN2': 0, 'GREEN3': 0, 'NDP2': 0, 'NDP3': 0, 'BLOC1': 0}
    >>> count_first_choices([g]*5 + [n]*3)
    {'GREEN1': 5, 'NDP1': 3, 'GREEN2': 0, 'GREEN3': 0, 'NDP2': 0, 'NDP3': 0, 'BLOC1': 0}
    >>> count_first_choices([['GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1'], ['GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1'], ['GREEN2', 'GREEN3', 'NDP1', 'NDP2', 'NDP3', 'BLOC1'], ['NDP1', 'NDP2', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3'], ['NDP1', 'NDP2', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3'], ['NDP1', 'NDP2', 'GREEN2', 'NDP3', 'BLOC1', 'NDP3']])
    {'GREEN2': 3, 'NDP1': 3, 'GREEN3': 0, 'NDP2': 0, 'NDP3': 0, 'BLOC1': 0}
    >>> count_first_choices([['GREEN3', 'NDP2', 'NDP3', 'BLOC1'], ['NDP2', 'NDP3', 'BLOC1', 'NDP3']])
    {'GREEN3': 1, 'NDP2': 1, 'NDP3': 0, 'BLOC1': 0}
    '''
    preferred_list = []
    if str(ballots) == '[]':
        return []
    else:
        for i in ballots:
            try:
                preferred_list.append(i[0])
            except IndexError:
                pass
        counted = count_plurality(preferred_list)

        # in case there are ones which didnt get any first pick votes we need to add them as parties with 0
        for j in flatten_lists(ballots):
            if j not in counted.keys():
                counted[j]=0
        return dict(counted)

if __name__ == '__main__':
    doctest.testmod()
