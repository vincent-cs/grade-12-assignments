# get file with data
def input_requested_help_rooms():
    requested_help_rooms = str(input('Enter a file name: ' ))
    # check just in case user has .txt at the end of their file
    if requested_help_rooms[-4:-1] == '.tx':
        return requested_help_rooms
    try:
        requested_help_rooms = int(requested_help_rooms)
        return ('test' + str(requested_help_rooms) + '.txt')
    except ValueError:
        modified_requested_help_rooms = requested_help_rooms + '.txt'
        return modified_requested_help_rooms

user_file = input_requested_help_rooms()
raw_room_data = open(user_file)
room_data = list(raw_room_data)
room_count = int(room_data[0])
# get rid of number of rooms from the data as we now are done with it, this is a convenient way of making the data simple to handle.
# with a larger data set this would be a poor way of handling this
del room_data[0]
# it is worth noting that we will be looking at the first and third index of each item in our list.

# now that we have our number of rooms, we can begin with a function to create an adjacency matrix
def create_adjacency_matrix(room_data):
    #start with adjacent rooms to the first, then the second and so on.
    adjacency_list = list('')
    room_to_analyze = 1
    while room_to_analyze <= room_count:
        found_connections_for_room = ''
        # iterate through all the items in data list to see if we can find an occurrence of the room we are looking at
        # once we have looked at all items in the list we will return the connected values which would be adjacent
        for index_to_analyze in room_data:
            found_connection = ''
            # search for the room number in each index
            where_connection_to_room_is = index_to_analyze.find(str(room_to_analyze))
            # if we can't find we are done and can move on to searching the next index
            # if we can find the room number we can take the other number and note that as a connection to that room
            if where_connection_to_room_is == 0:
                found_connection = index_to_analyze[2]
            elif where_connection_to_room_is == 2:
                found_connection = index_to_analyze[0]
                    # we've found the connection to the original room
            found_connections_for_room = found_connections_for_room + found_connection
        adjacency_list.append(found_connections_for_room)
        room_to_analyze = room_to_analyze + 1
    return adjacency_list

def check_valid_placement(output_from_generator):
    # if there are 2 tutors
    if len(output_from_generator) == 3:
        # we have to be careful with types here for comparisons to work
        all_rooms_connected = sorted(str(adjacency_matrix[int(output_from_generator[0])-1]) +
                                     str(adjacency_matrix[int(output_from_generator[2])-1]) +
                                     output_from_generator[0] + output_from_generator[2])
        rooms_found = 0
        for unique_room in range(1, room_count + 1):
            # loop through all the numbers to see if all the rooms are accounted for
            if str(unique_room) in all_rooms_connected:
                rooms_found = rooms_found + 1
                if rooms_found == room_count:
                    print('Tutors needed: 2')
                    print('Tutors assigned to rooms: ' + output_from_generator[0] + ', ' + output_from_generator[2])
                    quit()

    # check if we are looking at the possibility of 3 tutors?
    elif len(output_from_generator) == 5:
        all_rooms_connected = sorted(str(adjacency_matrix[int(output_from_generator[0])-1]) +
                                     str(adjacency_matrix[int(output_from_generator[2])-1]) +
                                     str(adjacency_matrix[int(output_from_generator[4])-1]) +
                                     output_from_generator[0] + output_from_generator[2] +
                                     output_from_generator[4])
        # do first test, if that proceeds we can do second one after adjusting the values slightly.
        rooms_found = 0
        for unique_room in range(1, room_count + 1):
            # loop through all the numbers to see if all the rooms are accounted for
            if str(unique_room) in all_rooms_connected:
                rooms_found = rooms_found + 1
                if rooms_found == room_count:
                    print('Tutors needed: 3')
                    print('Tutors assigned to rooms: ' + output_from_generator[0] + ', ' + output_from_generator[2] + ', ' + output_from_generator[4])
                    quit()

adjacency_matrix = create_adjacency_matrix(room_data)
# formatting is ['2', '13', '24', '35', '46', '5']
# each item in the list corresponds to its respective room number. i.e. the 3rd item ('24') here indicates that room 3 connects to room 2 and room 4.
# this formatting would need to be refined if there was a double digit amount of rooms.

# this program is designed to only look at 2 or 3 tutors for simplicity, however this could be looped.
look_at_2tutors = True
while True:
    # generate numbers, change behaviour if we are dealing with a third tutor not just 2.
    # syntax looks like '1 3 6' for a tutor in room 1, 3 and 6, respectively
    for num1 in range(1, room_count + 1):
        for num2 in range(1, room_count + 1):
            if look_at_2tutors == True:
                check_valid_placement(str(num1) + ' ' + str(num2)) # give to test function
                if (str(num1) + ' ' + str(num2)) == '6 6':
                    look_at_2tutors = False
            else:
                for num3 in range(1, room_count + 1):
                    check_valid_placement(str(num1) + ' ' + str(num2) + ' ' + str(num3))  # give to test function
                    if str(str(num1) + ' ' + str(num2) + ' ' + str(num3)) == '6 6 6':
                        print('All possibilities processed')
                        print('Please note that this program supports up to 3 tutors and 9 rooms.')
                        quit()