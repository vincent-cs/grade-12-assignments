def flatten_lists(nested):
    '''
    (list) -> list

    Input a list and replaces any list inside with their values, return the
    new list.

    >>> flatten_lists([[0],[1, 2], 3])
    [0, 1, 2, 3]
    >>> flatten_lists([2, 4, [1, 8], 3])
    [2, 4, 1, 8, 3]
    '''
    flattened_list = []
    for i in nested:
        if type(i) == list:
            for j in i:
                flattened_list.append(j)
        else:
            flattened_list.append(i)
    return flattened_list