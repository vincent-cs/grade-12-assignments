def check_prime(n):
    # Corner cases
    if (n <= 1):
        return False
    if (n <= 3):
        return True

    # This is checked so that we can skip
    # middle five numbers in below loop
    if (n % 2 == 0 or n % 3 == 0):
        return False

    i = 5
    while (i * i <= n):
        if (n % i == 0 or n % (i + 2) == 0):
            return False
        i = i + 6
    return True


# set this to see how far the program will go (in percentage of prime to total diagonals)
target_ratio = 10

counter = 1
numbers_to_advance = 2

# we need to count 1 as the first composite
prime_count = 0
composite_count = 1

while True:
    mini_counter = 1
    # to do this correctly we need a pattern which repeats itself.
    # In this case, we are running this 4 times before increasing the number as the spiralling continues.
    while mini_counter < 5:
        if check_prime(counter + numbers_to_advance) == True:
            prime_count = prime_count + 1
        else:
            composite_count = composite_count + 1
        mini_counter = mini_counter + 1
        counter = counter + numbers_to_advance
    numbers_to_advance = numbers_to_advance + 2

    # output
    if composite_count > 100:
        ratio = float((prime_count / (prime_count + composite_count)) * 100)
        if ratio <= target_ratio:
            print('Calculation completed for target ratio of below ' + str(target_ratio) + '% of prime numbers to total diagonal numbers.')
            print('Numbers added to spiral: ' + str(counter))
            print('Composite count: ' + str(composite_count))
            print('Prime count: ' + str(prime_count))
            print('Found ratio (not rounded): ' + str(ratio) + '%')
            print('Side length: ' + str(numbers_to_advance - 1))
            quit()
        #else:
            #useful to see how the program is progressing
            #print(str(round(ratio, 2)) + '%')

