import math
def perfect_square_check(num):
    if num == int((math.sqrt(num)) + 0.5) ** 2:
        return True
    else:
        return False

D_upper_limit = 1000 # instructions specify 1000
y_upper_limit = 100000 # no specific amount from instructions

D_list = []
x_list = []
y_list = []
D_not_found = []
last_D_value_found = 0

for D in range (1, D_upper_limit):
    if perfect_square_check(D) == True or D == last_D_value_found:
        if D not in D_not_found:
            D_not_found.append(D)
        D = D + 1
    for y in range(1, y_upper_limit):
        x_found = math.sqrt(1 + D * y ** 2) # solving for x instead of trying thousands of values saves lots of time
        try:
            x_check = int(x_found) # if it is an int we can use it, if not move on.
        except ValueError:
            last_D_value_found = D
        if (x_check ** 2 - (D * y ** 2)) == 1 and D > last_D_value_found: # last check can prevent weird unecessary runs
            D_list.append(D)
            x_list.append(x_check)
            y_list.append(y)
            last_D_value_found = D # set this so we stop looking for the particular D value once solutions are found
    if D not in D_list and D not in D_not_found: # use this to see if our y needs to be increased
        D_not_found.append(D)
        #print(D_not_found)



# how to differ between 7 127 48 and 7 8 3
# we want 7 8 3 as that has a minimal y

list_of_highest_x = []
list_of_lowest_y = []
list_of_lowest_D = []

# first find the minimal solution for each index and find the respective x value from each number
# once we have found all the valid x values (solutions where the y is the smallest for that D) we can pick the biggest X
# That biggest X will then lead us back to finding what D value that may have corresponded too

# look through each index to find the minimal solution, and then note its x
for p in range(2, D_upper_limit):
    # first note all the times this D value appears
    indexes_where_this_D_can_be_found = [i for i, j in enumerate(D_list) if j == p]
    #print('indexes of D ' + str(indexes_where_this_D_can_be_found))
    #if indexes_where_this_D_can_be_found == []:
    try:
        list_of_different_y_values_for_certain_D = []
        # now we can search through to find the one with the lowest y so we have the minimal solution for that D
        for k in indexes_where_this_D_can_be_found:
            list_of_different_y_values_for_certain_D.append(y_list[k])

        lowest_y = min(list_of_different_y_values_for_certain_D)

        indexes_for_x_with_lowest_y_for_certain_D = [i for i, j in enumerate(y_list) if j == lowest_y]
        # now that we know where the xs can be found we can note all of them and then note the max
        #print('indexes_for_x_with_lowest_y_for_certain_D' + indexes_for_x_with_lowest_y_for_certain_D)
        list_of_different_x_values = []
        for o in indexes_for_x_with_lowest_y_for_certain_D:
            list_of_different_x_values.append(x_list[o])
        highest_x_value = max(list_of_different_x_values)

        # use this x value to locate the relevant D value
        list_of_different_D_values = []
        indexes_for_D_with_highest_x_for_lowest_y_for_certain_D = [i for i, j in enumerate(x_list) if j == highest_x_value]
        for w in indexes_for_D_with_highest_x_for_lowest_y_for_certain_D:
            list_of_different_D_values.append(D_list[w])
        lowest_D = min(list_of_different_D_values)


        list_of_highest_x.append(highest_x_value)
        list_of_lowest_y.append(lowest_y)
        list_of_lowest_D.append(lowest_D)
    except ValueError:
         a1 = 1

# Take these lists of values which give all the minimal solutions, and find the biggest x value.

max_x_value = max(list_of_highest_x)

indexes_of_max_x_value = [i for i, j in enumerate(list_of_highest_x) if j == max_x_value]

# we can now find the corresponding minimum D value
new_list_of_different_D_values = []
for v in indexes_of_max_x_value:
    new_list_of_different_D_values.append(list_of_lowest_D[v])
min_D_value = min(new_list_of_different_D_values)

# finally we can find the corresponding minimum y value
indexes_of_min_y_values = [i for i, j in enumerate(list_of_lowest_D) if j == min_D_value]

# todo: clean up this area as this calculation is making incorrect assumptions as to where the lowest y value actually is
new_list_of_different_y_values = []
for a in indexes_of_min_y_values:
    new_list_of_different_y_values.append(list_of_lowest_y[a])
min_y_value = min(new_list_of_different_y_values)

# solve for y
y_solved = math.sqrt((1 - max_x_value ** 2)/(-min_D_value))

# check if the D values we are not found should be found (i.e. are they perfect squares, if so that is ok we didn't find them).
D_not_found_no_perfect_squares = []
D_not_found_with_perfect_squares = []
for i in D_not_found:
    if perfect_square_check(i) == False:
        D_not_found_no_perfect_squares.append(i)
    else:
        D_not_found_with_perfect_squares.append(i)


print('D: ' + str(min_D_value))
print('x: ' + str(max_x_value))
print('y: ' + str(int(y_solved)))
print('Thus: ' + str(max_x_value) + '^2 - ' + str(min_D_value) + ' * ' + str(int(y_solved)) + '^2 = 1')
print('')
print('D values not found (all): ' + str(len(D_not_found)))
print(D_not_found)
print('')
print('D values not found (only perfect squares): ' + str(len(D_not_found_with_perfect_squares)))
print(D_not_found_with_perfect_squares)
print('')
print('D values not found (not including perfect squares): ' + str(len(D_not_found_no_perfect_squares)))
print(D_not_found_no_perfect_squares)
#print('Other (sometimes incorrect) calculation of y: ' + str(min_y_value))
