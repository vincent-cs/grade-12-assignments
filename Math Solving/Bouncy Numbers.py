count = 0
for i in range(1, 10000000):
    if list(str(i)) != sorted(str(i)) and list(str(i)) != sorted(str(i), reverse=True):
        count = count + 1
    if (count / i) == 0.99:
        print('Total numbers: ' + str(i))
        print('Bouncy numbers: ' + str(count))
        print('Proportion percentage: ' + str(100 * (count / i)) + '%')
        quit()
