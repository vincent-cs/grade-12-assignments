import math
counter = 0

for n in range(1, 101):
    for r in range(1, 101):
        if r <= n and (int(math.factorial(n)/(math.factorial(r)*math.factorial(n-r)))) > 1000000:
            counter = counter + 1
print(counter)