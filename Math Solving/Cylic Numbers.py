'''
def check_if_same(number, multiplier):
    multiplied = number * multiplier
    if sorted(str(number)) == sorted(str(multiplied)):
        return multiplier

# generate numbers
for number in range(588235294117647, 588235294117648): # start at 10 as first 9 digits are wrong
    valid_multipliers = []
    for multiplier in range(2, len(str(number)) + 1):
        valid_multipliers.append(check_if_same(number, multiplier))
    if len(valid_multipliers) == len(str(number)) - 1 and None not in valid_multipliers:
        print(str(number) + ' is cyclic.')
        print(valid_multipliers)

'''
import numpy as np
from decimal import Decimal

def check_prime(n):
    # Corner cases
    if (n <= 1):
        return False
    if (n <= 3):
        return True

    # This is checked so that we can skip
    # middle five numbers in below loop
    if n % 2 == 0 or n % 3 == 0:
        return False

    i = 5
    while (i * i <= n):
        if (n % i == 0 or n % (i + 2) == 0):
            return False
        i = i + 6
    return True


# 60 of them up to 1000
#full_reptend_primes = [7, 17, 19, 23, 29, 47, 59, 61, 97, 109, 113, 131, 149, 167, 179, 181, 193, 223, 229, 233, 257, 263, 269, 313, 337, 367, 379, 383, 389, 419, 433, 461, 487, 491, 499, 503, 509, 541, 571, 577, 593, 619, 647, 659, 701, 709, 727, 743, 811, 821, 823, 857, 863, 887, 937, 941, 953, 971, 977, 983]
# from https://oeis.org/A001913

#from http://www.rosettacode.org/wiki/Long_primes#Python
# while one could also calculate it by solving the equation and looking at its length, this method is much faster
def find_period(n): # outputs len of the period of the number, i.e. 12345621234562 would be 7
    r = 1
    for i in range(1, n): r = (10 * r) % n
    rr = r
    period = 0
    while True:
        r = (10 * r) % n
        period += 1
        if r == rr: break
    return period

def format_float(num):
    return np.format_float_positional(num, trim='-')
#https://stackoverflow.com/questions/658763/

#counter = 0
#for num in range(3, 10000, 2):
#for num in range(3, 10000, 2):
#for num in range(1, 100, 2):
for num in range(210000000, 3000000000, 2):
    if check_prime(num) == True and num - 1 == find_period(num):
            reciprocal = format_float(1 / (num)) # todo how to get these to divide with the precision we needto answer the question?
            #print(num)
            #print(1/Decimal(num))
            #print(str(1/num))
            #print(reciprocal)
            modified_reciprocal = (str(reciprocal)[2:num+1])
            print((modified_reciprocal))
            #print(modified_reciprocal[-1])

            if str(modified_reciprocal)[0:11] == '00000000137':
                #and str(modified_reciprocal)[-5:-1] == '5678' and str(modified_reciprocal)[
                #-1] == '9':
                print(modified_reciprocal + ' is desired cyclic.')
                print(reciprocal)
                quit()

print('Gone through ' + str(num) + ' numbers.')

