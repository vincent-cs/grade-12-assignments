# taking input from user
def check_prime(number):

    # prime number is always greater than 1
    if number > 1:
        for i in range(2, number):
            if (number % i) == 0:
                return False
                break
        else:
            return True

    # if the entered number is less than or equal to 1
    # then it is not prime number
    else:
        return False

list_of_primes = []
list_of_composites = []
for i in range(1, 10000):
    if check_prime(i) == True:
        list_of_primes.append(i)
    else:
        list_of_composites.append(i)


myList = []
for n1 in list_of_composites:
    for n2 in list_of_primes:
        for n3 in range(1, 60):
            if n1 == n2 + 2 * n3 ** 2 and n1 % 2 != 0:
                myList.append(n1)
                n1 = n1 + 1

for n4 in range(1, 10000):
    if check_prime(n4) == False and n4 not in myList and n4 % 2 != 0 and n4 != 1:
        print('Solution is ' + str(n4))
        quit()


