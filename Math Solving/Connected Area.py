
from connected_area_helper import *

from itertools import permutations, chain, islice
import numpy as np
from sympy.utilities.iterables import multiset_permutations




# find all the levels that the squares can be filled as matrices.
def find_different_amounts_of_coloured_squares(length, coloured):

    matrix = [[0 for x in range(length)] for y in range(length)]
    count = 0
    for row in matrix:
        for square in row:
            if count < coloured:
                row[row.index(square)] = 1
                count+=1
    #print(matrix)
    #print(np.array(matrix))
    return np.array(matrix)
    #return matrix

def find_all_permutations_for_certain_coloured(square_with_certain_amount_coloured):

    all_perms = np.array(square_with_certain_amount_coloured)
    certain_combo = np.array(square_with_certain_amount_coloured)

    for perm in multiset_permutations(certain_combo):
        all_perms = np.vstack((all_perms, (perm)))
    new = np.delete(all_perms, (0),
                    axis=0)  # the first one is put there so the calculation (vstack) works, but now that we are done finding perms we get rid of the first
    return new


length = 2

all_combos = []
square_cells = 1 # we are avoiding calculating if there is 0
connected_cells = 0
square_with_certain_amount_coloured = []

# https://stackoverflow.com/a/41210450/10973516


for i in range(1, length ** 2 + 1):
    square_with_certain_amount_coloured = (find_different_amounts_of_coloured_squares(length, i))
    # gives matrix
    # [[1 0]
    #  [0 0]]

    # [[1, 0], [0, 0]]
    # [[1, 1], [0, 0]]
    # [[1, 1], [1, 0]]
    # [[1, 1], [1, 1]]
    #print(square_with_certain_amount_coloured)
    #square_with_certain_amount_coloured = flatten_lists(square_with_certain_amount_coloured)
    #square_with_certain_amount_coloured = np.array(square_with_certain_amount_coloured)
    print(square_with_certain_amount_coloured)
    # [1 0 0 0]
    # [1 1 0 0]
    # [1 1 1 0]
    # [1 1 1 1]

    #all_permutations_for_certain_coloured = find_all_permutations_for_certain_coloured(square_with_certain_amount_coloured)
    # [[0 0 0 1]
    #  [0 0 1 0]
    #  [0 1 0 0]
    #  [1 0 0 0]]
    # now that we have a list a of a certain amount coloured we can convert it back to a matrix so we can find connected cells
    #print((all_permutations_for_certain_coloured))






    #print(square_with_certain_amount_coloured)








if __name__ == "__main__":
    import doctest
    doctest.testmod()