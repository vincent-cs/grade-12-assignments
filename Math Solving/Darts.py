multiplier_reference = ['ZERO', 'S', 'D', 'T']
combo_counter = 0
combo_storage = []

# while the way this loops could probably be made a lot more efficiently or even compactly, this is a clean way of doing it while ensuring all the possibilities are found.
for score in range(1, 100): #216 000 possibilities per score with our program (21 384 000 checks total), we do not attempt to shave off time as this is fairly quick already
    for region1 in range(1, 21):
        for multiplier1 in range(0, 4):
            for region2 in range(1, 21):
                for multiplier2 in range(0, 4):
                    for region3 in range(1, 21):
                        for multiplier3 in range(0, 4):
                            darts_combo = []  # important
                            darts_metadata = []  # needed to see if we are making valid checkouts
                            darts_clean = []  # this is also handy

                        # these could be looped to save space however would add unnecessary complexity to simple logic
                            darts_combo.append(multiplier_reference[multiplier1] + str(region1))
                            darts_combo.append(multiplier_reference[multiplier2] + str(region2))
                            darts_combo.append(multiplier_reference[multiplier3] + str(region3))

                            darts_clean.append(region1)
                            darts_clean.append(region2)
                            darts_clean.append(region3)

                            darts_score = multiplier1 * region1 + multiplier2 * region2 + multiplier3 * region3

                            darts_metadata.append(multiplier_reference[multiplier1])
                            darts_metadata.append(multiplier_reference[multiplier2])
                            darts_metadata.append(multiplier_reference[multiplier3])

                            # there is a posssibility things will be 0 (i.e. they didnt need to throw 3,
                            #only 2 darts to complete checkout, if so our program needs to handle that.
                            indices_to_prune = [i for i, x in enumerate(darts_metadata) if x == 'ZERO']
                            if len(indices_to_prune) > 0:
                                for i in range(2, -1, -1): # go backwards otherwise things may move while we prune the list
                                    if i in indices_to_prune:
                                        darts_combo.pop(i)

                            # we also need to note that if we reverse the first and second throws (but not third)
                            # it still counts as the same checkout.
                            # i.e. ['T1', 'D1', 'D1'] counts for one, ['D1', 'T1', 'D1'] counts for the same one.
                            saved_darts_combo = darts_combo
                            reordered_darts_combo = darts_combo.copy()
                            if len(reordered_darts_combo) == 3:
                                reordered_darts_combo[0], reordered_darts_combo[1] = reordered_darts_combo[1], \
                                                                                     reordered_darts_combo[0]
                            # list[pos1], list[pos2] = list[pos2], list[pos1]
                            if darts_score == score and darts_metadata[-1] == 'D' and darts_combo not in combo_storage\
                                    and reordered_darts_combo not in combo_storage:
                                #print('Combo found: ' + str(darts_combo))
                                combo_storage.append(darts_combo) # we need to ensure we do not make save everything
                                combo_counter = combo_counter + 1

    print('Looking at score: ' + str(score))
print(combo_counter)
