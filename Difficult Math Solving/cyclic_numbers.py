'''
def check_if_same(number, multiplier):
    multiplied = number * multiplier
    if sorted(str(number)) == sorted(str(multiplied)):
        return multiplier

# generate numbers
for number in range(588235294117647, 588235294117648): # start at 10 as first 9 digits are wrong
    valid_multipliers = []
    for multiplier in range(2, len(str(number)) + 1):
        valid_multipliers.append(check_if_same(number, multiplier))
    if len(valid_multipliers) == len(str(number)) - 1 and None not in valid_multipliers:
        print(str(number) + ' is cyclic.')
        print(valid_multipliers)

'''

from decimal import *
import decimal
#import numpy as np
#from itertools import groupby

def check_prime(n):
    # Corner cases
    if (n <= 1):
        return False
    if (n <= 3):
        return True

    # This is checked so that we can skip
    # middle five numbers in below loop
    if n % 2 == 0 or n % 3 == 0:
        return False

    i = 5
    while (i * i <= n):
        if (n % i == 0 or n % (i + 2) == 0):
            return False
        i = i + 6
    return True


# 60 of them up to 1000
#full_reptend_primes = [7, 17, 19, 23, 29, 47, 59, 61, 97, 109, 113, 131, 149, 167, 179, 181, 193, 223, 229, 233, 257, 263, 269, 313, 337, 367, 379, 383, 389, 419, 433, 461, 487, 491, 499, 503, 509, 541, 571, 577, 593, 619, 647, 659, 701, 709, 727, 743, 811, 821, 823, 857, 863, 887, 937, 941, 953, 971, 977, 983]
# from https://oeis.org/A001913

#from http://www.rosettacode.org/wiki/Long_primes#Python
# while one could also calculate it by solving the equation and looking at its length, this method is much faster

# there are other ways to calculate whether a number is cyclic or not, however this is the only way I found which could reliably produce results
# for full reptend primes of up to 1000 (there should be 60)
def find_period(n): # outputs len of the period of the number, i.e. 12345621234562 would be 7
    r = 1
    for i in range(1, n):
        r = (10 * r) % n
    rr = r
    period = 0
    while True:
        r = (10 * r) % n
        period += 1
        if r == rr:
            break
    return period


# note that sometimes the pattern of zeros change, it stops increasing for a bit.
# 999983 > 1000171 (1 milllion) the zeros stop go from 5 zeros to none, but the pattern continue later.

# note that sometimes the pattern of zeros change, it stops increasing for a bit. (

#counter = 0
#for num in range(3, 10000, 2):
#for num in range(7, 1000, 2): # starting at 8 so we can assume there are always zeros we need to add
#for num in range(1, 1000, 2):
#for num in range(999961, 1000193, 2):
#for num in range(10000000001, 10000000000000001, 2):
    if check_prime(num) == True and find_period(num) == num - 1: # find full reptend primes
        #print('prime: ' + str(num))
        getcontext().prec = num + 1
        #new_reciprocal = Context(rounding=None, prec=100000000, Emax=1000000000).divide(1, num)
        #print(str(Context().canonical(new_reciprocal))[2:11])
        reciprocal = (1 / Decimal(num))
        #print(str(reciprocal)[2:num+1])
        print(str(reciprocal)[2:11])
        #print(str(new_reciprocal)[2:11])
        #print('')


            #cyclic = (((10 ** (num - 1)) - 1) / Decimal(num))
            #print('Cyclic: (no zeros): ' + str(cyclic))

            #print('Cyclic from reciprocal: ' + str((modified_reciprocal)))
            #print(str(modified_reciprocal)[0:11])

            # if str(modified_reciprocal)[0:11] == '00000000137':
            #     #and str(cyclic)[-5:-1] == '5678' and str(cyclic)[-1] == '9':
            #     print(modified_reciprocal + ' is desired cyclic.')
            #     print('Prime: ' + str(num))
            #     #print('Cyclic: (no zeros): ' + str(cyclic))
            #     print('Cyclic (beginning with zeros): ' + str((reciprocal)))
            #     quit()

#print('Gone through ' + str(num) + ' numbers.')

# old attempts
# getcontext().prec = 11 # set the calculations to be a certain length.
        # reciprocal = (1 / (num))
        # #print(reciprocal)
        # #modified_reciprocal = (str(reciprocal)[2:num + 1])  # we need to check via reciprocal for the zeros to be valid
        # #print(str(reciprocal)[2:13]) # we need to check via reciprocal for the zeros to be valid
        # #print(modified_reciprocal)
        # # count 0s
        #
        # groups = groupby(str(reciprocal))
        # result = [(label, sum(1 for _ in group)) for label, group in groups]
        #
        # zeros_to_add = result[0][1]
        # print(zeros_to_add)
        #
        # getcontext().prec = num - 1  # set the calculations to be a certain length.
        # cyclic = (((10 ** (num - 1)) - 1) / Decimal(num))
        # print('Cyclic: (no zeros): ' + str(cyclic))
        #
        # cyclic_complete = '0' * zeros_to_add + str(cyclic)
        # print('Cyclic: (with zeros): ' + str(cyclic_complete))