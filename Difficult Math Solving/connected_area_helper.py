#For the Random Connected Area - expected values is total the sum of the total individual max connected areas dividied by the number of squares.
#For the example given, we have 6 squares with max of 1 connected area, 4 with a max of 2 connected areas, 4 with a max of 3 connected areas, and 1 with a max of 4 connected areas.

#(6x1) + (4x2) + (4x3) + (1x4) = 30

#30/16 = 1.875



from itertools import permutations, chain, islice
import numpy as np
from sympy.utilities.iterables import multiset_permutations

def flatten_lists(nested):
    '''
    (list) -> list

    Input a list and replaces any list inside with their values, return the
    new list.

    >>> flatten_lists([[0],[1, 2], 3])
    [0, 1, 2, 3]
    >>> flatten_lists([2, 4, [1, 8], 3])
    [2, 4, 1, 8, 3]
    '''
    flattened_list = []
    for i in nested:
        if type(i) == list:
            for j in i:
                flattened_list.append(j)
        else:
            flattened_list.append(i)
    return flattened_list

# https://stackoverflow.com/a/21960089/
def uniperm_arrays(arr):
    flat = chain.from_iterable(arr)
    perms = set(permutations(flat))
    for perm in perms:
        pit = iter(perm)
        yield [list(islice(pit, len(row))) for row in arr]


def find_adjacencies_of_permutation(permutation):

    '''
    >>> find_adjacencies_of_permutation([[1, 0], [0, 1]])
    1
    >>> find_adjacencies_of_permutation([[1, 0], [1, 0]])
    2
    >>> find_adjacencies_of_permutation([[1, 1], [1, 0]])
    3
    >>> find_adjacencies_of_permutation([[1, 1], [1, 1]])
    4
    >>> find_adjacencies_of_permutation([[1, 1], [0, 0]])
    2
    >>> find_adjacencies_of_permutation([[1, 0], [0, 0]])
    1
    >>> find_adjacencies_of_permutation([[1, 1, 0], [0, 1, 0], [0, 1, 0]])
    4
    >>> find_adjacencies_of_permutation([[1, 1, 0], [1, 0, 0], [1, 0, 1]])
    4
    >>> find_adjacencies_of_permutation([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
    9
    >>> find_adjacencies_of_permutation([[0, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]])
    3
    '''

    # there are only 4 possible directions another 1 could be from our coloured (up, down, left or right)
    directions = [[0, 1], [0, -1], [-1, 0], [1, 0]] # x and y coords relative to our current value we are looking at
    adjacencies_found = []
    for row in permutation:
        for square in row:
            if square == 1:
                original_indexes_square = [i for i, x in enumerate(row) if x == square] # the square is equivalent to the column, however we refer to it as square
                original_indexes_row = [i for i, x in enumerate(permutation) if x == row]
                #print('original indexes square: ' + str(original_indexes_square))
                #print('original indexes row: ' + str(original_indexes_row))

                for way in directions: # look at all 4 possible connections that there might be
                    #print('Looking at way: ' + str(way))
                    for square_num in original_indexes_square: # for what x
                        for row_num in original_indexes_row: # for what y (higher row_num means lower row in matrix)

                            # finding indexes adjacent to our original 1 (var: square)
                            # we will then check if these are also 1, if so that means we have found a connection

                            index_to_compare_square = square_num + way[0]
                            index_to_compare_row = row_num + way[1] # while increasing x will move to the right correctly, this will not be the case for y
                            # this may get us negative indexes, which would not be processed in the way the program expects.
                            # thus any negative indexes will not be dealt with.

                            try:
                                if permutation[index_to_compare_row][index_to_compare_square] == 1 and\
                                        index_to_compare_square >= 0 and index_to_compare_row >= 0 and\
                                        [square_num, row_num, index_to_compare_square, index_to_compare_row]\
                                        not in adjacencies_found and\
                                        [index_to_compare_square, index_to_compare_row, square_num, row_num] not in adjacencies_found:

                                    adjacencies_found.append([square_num, row_num])
                                    adjacencies_found.append([index_to_compare_square, index_to_compare_row])
                                    # note that we have found 2 different squares by establishing this connection.
                                    # However, we will later check how many times we found a unique square in particular, not how many times we counted that square.

                                    #print('Square (column) ' + str(square_num) + ', row ' + str(row_num) + ' is coloured. ')
                                    #print('Square (column) ' + str(index_to_compare_square) + ', row ' + str(index_to_compare_row) + ' is also coloured.')

                            except IndexError:
                                pass

    # find unique items in the list as we may have counted a single square multiple times
    unique = [x for i, x in enumerate(adjacencies_found) if i == adjacencies_found.index(x)]

    # find if we have actually no colours (all zeros) in our permutation (note: if this function is slowing us down, remove this is it is doing a rather unecessary check)

    if unique == []: # with zero connections, but with a 1 present somewhere, we can still have an area of one.
        return 1
    else:
        return len(unique)

def find_permutations(length, coloured):
    '''
    >>> find_permutations(4, 1)
    '''

    matrix = [[0 for x in range(length)] for y in range(length)]
    count = 0
    for row in matrix:
        for square in row:
            if count < coloured:
                row[row.index(square)] = 1
                count+=1
   # print(matrix)

    var = list(uniperm_arrays(matrix))
    #find_all_permutations_for_certain_coloured()
    return var


length = 2
all_combos = []
square_cells = 1 # we are avoiding calculating if there is 0
connected_cells = 0


# https://stackoverflow.com/a/41210450/10973516




for i in range(1, length ** 2 + 1):
    all_permutations = (find_permutations(length, i))
    for permutation in all_permutations:
        square_cells += 1
    #     connected_cells += find_adjacencies_of_permutation(permutation)
#print(len(all_permutations))
print(square_cells)
#print(connected_cells/square_cells)

# the amount of permutatiosn for all the levels of completness is 16 for 4 squares
# this is equal to 2^4
# for 9 squares it is 2^9
# for 16 is must be 2^16

if __name__ == "__main__":
    import doctest
    doctest.testmod()