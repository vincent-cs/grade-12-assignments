Generally I found this assignment to be straightforward, despite conversions being confusing at times.
Listing conversions one at a time made this process clearer, and was not converted into one line as that will only make it more difficult to read.

There was the function fp_from_coach in footprint_transport.py which did not come with proper doctests for it.
Rather it was testing the function before it, fp_from_train.
I did not tamper with this as it seemed nothing of value was lost, as the calculations themselves were following the same rigour every other function.
Changing the tests to be properly representative of the function would therefore not have benefited the user or myself.

Perhaps one error could be that not entering any input into the .csv file leads to errors, however this could be fixed by simply putting zeros in the default example.csv file, for users then to change.
There is a second benefit of making it clearer to where user input is to be entered (if a graphical interface is not made).

There was also the odd INCOMPLETE = -1 on every file, I guessed that to be for marking or something.