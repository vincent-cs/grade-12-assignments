
import doctest
from unit_conversion import *

INCOMPLETE = -1

######################################
def fp_from_online_use(daily_online_use_hours):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E from being online

    >>> fp_from_online_use(0)
    0.0
    >>> round(fp_from_online_use(2), 4)
    0.0402
    >>> round(fp_from_online_use(13), 4)
    0.2611
    '''
    # convert between units of mass
    fp_from_online_use_daily_grams = daily_online_use_hours * 55
    fp_from_online_use_daily_kg = fp_from_online_use_daily_grams/1000
    fp_from_online_use_daily_tonnes = kg_to_tonnes(fp_from_online_use_daily_kg)
    # convert from daily to annual
    fp_from_online_use_annual_tonnes = daily_to_annual(fp_from_online_use_daily_tonnes)
    return fp_from_online_use_annual_tonnes

def fp_from_phone_use(daily_phone_use_hours):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E from amount of time on phone

    >>> fp_from_phone_use(0)
    0.0
    >>> round(fp_from_phone_use(2), 4)
    2.5
    >>> round(fp_from_phone_use(13), 4)
    16.25
    '''
    # convert between units of mass
    fp_from_phone_use_annual_kg = daily_phone_use_hours * 1250
    fp_from_phone_use_annual_tonnes = kg_to_tonnes(fp_from_phone_use_annual_kg)
    return fp_from_phone_use_annual_tonnes

def fp_from_small_devices(new_small_devices):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of using small portable devices

    >>> fp_from_small_devices(0)
    0.0
    >>> round(fp_from_small_devices(2), 4)
    0.15
    >>> round(fp_from_small_devices(13), 4)
    0.975
    '''
    # convert between units of mass
    fp_from_small_devices_kg = new_small_devices * 75
    fp_from_small_devices_tonnes = kg_to_tonnes(fp_from_small_devices_kg)
    return fp_from_small_devices_tonnes

def fp_from_medium_devices(new_medium_devices):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of using medium-sized devices

    >>> fp_from_medium_devices(0)
    0.0
    >>> round(fp_from_medium_devices(2), 4)
    0.4
    >>> round(fp_from_medium_devices(13), 4)
    2.6
    '''
    # convert between units of mass
    fp_from_medium_devices_kg = new_medium_devices * 200
    fp_from_medium_devices_tonnes = kg_to_tonnes(fp_from_medium_devices_kg)
    return fp_from_medium_devices_tonnes

def fp_from_large_devices(new_large_devices):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of using large/heavy devices

    >>> fp_from_large_devices(0)
    0.0
    >>> round(fp_from_large_devices(2), 4)
    1.6
    >>> round(fp_from_large_devices(13), 4)
    10.4
    '''
    # convert between units of mass
    fp_from_large_devices_kg = new_large_devices * 800
    fp_from_large_devices_tonnes = kg_to_tonnes(fp_from_large_devices_kg)
    return fp_from_large_devices_tonnes

######################################

def fp_of_computing(daily_online_use, daily_phone_use, new_small_devices, new_medium_devices, new_large_devices):
    '''(num, num) -> float

    Metric tonnes of CO2E from computing, based on daily hours of online & phone use, and how many small (phone/tablet/etc) & large (laptop) & workstation devices you bought.

    Source for online use: How Bad Are Bananas
        55 g CO2E / hour

    Source for phone use: How Bad Are Bananas
        1250 kg CO2E for a year of 1 hour a day

    Source for new devices: How Bad Are Bananas
        200kg: new laptop
        800kg: new workstation
        And from: https://www.cnet.com/news/apple-iphone-x-environmental-report/
        I'm estimating 75kg: new small device

    >>> fp_of_computing(0, 0, 0, 0, 0)
    0.0
    >>> round(fp_of_computing(6, 0, 0, 0, 0), 4)
    0.1205
    >>> round(fp_of_computing(0, 1, 0, 0, 0), 4)
    1.25
    >>> fp_of_computing(0, 0, 1, 0, 0)
    0.075
    >>> fp_of_computing(0, 0, 0, 1, 0)
    0.2
    >>> fp_of_computing(0, 0, 0, 0, 1)
    0.8
    >>> round(fp_of_computing(4, 2, 2, 1, 1), 4)
    3.7304
    '''
    return fp_from_online_use(daily_online_use) + fp_from_phone_use(daily_phone_use) + fp_from_small_devices(new_small_devices) + fp_from_medium_devices(new_medium_devices) + fp_from_large_devices(new_large_devices)

######################################
def fp_from_meat(daily_meat_grams):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of the consumption of meat

    >>> fp_from_meat(0)
    0.0
    >>> round(fp_from_meat(2), 4)
    0.0196
    >>> round(fp_from_meat(13), 4)
    0.1273
    '''
    # convert from different units of mass
    fp_from_meat_daily_grams = daily_meat_grams * 26.8
    fp_from_meat_daily_kg = fp_from_meat_daily_grams/1000
    fp_from_meat_daily_tonnes = kg_to_tonnes(fp_from_meat_daily_kg)
    # convert to annual frp, daily
    fp_from_meat_annual_tonnes = daily_to_annual(fp_from_meat_daily_tonnes)
    return fp_from_meat_annual_tonnes

def fp_from_cheese(daily_cheese_grams):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of the consumption of cheese

    >>> fp_from_cheese(0)
    0.0
    >>> round(fp_from_cheese(2), 4)
    0.0088
    >>> round(fp_from_cheese(13), 4)
    0.057
    '''
    # convert from different units of mass
    fp_from_cheese_daily_grams = daily_cheese_grams * 12
    fp_from_cheese_daily_kg = fp_from_cheese_daily_grams / 1000
    fp_from_cheese_daily_tonnes = kg_to_tonnes(fp_from_cheese_daily_kg)
    # convert to annual frp, daily
    fp_from_cheese_annual_tonnes = daily_to_annual(fp_from_cheese_daily_tonnes)
    return fp_from_cheese_annual_tonnes

def fp_from_milk(daily_milk_litres):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of the consumption of milk in L

    >>> fp_from_milk(0)
    0.0
    >>> round(fp_from_milk(2), 4)
    0.1956
    >>> round(fp_from_milk(13), 4)
    1.2714
    '''
    # convert between units of mass
    fp_from_milk_daily_grams = daily_milk_litres * 267.7777
    fp_from_milk_daily_kg = fp_from_milk_daily_grams / 1000
    fp_from_milk_daily_tonnes = kg_to_tonnes(fp_from_milk_daily_kg)
    # convert from daily to annual
    fp_from_milk_annual_tonnes = daily_to_annual(fp_from_milk_daily_tonnes)
    return fp_from_milk_annual_tonnes

def fp_from_eggs(daily_eggs):
    '''
    (num) -> flt
    Estimate metric tonnes of CO2E of the consumption of eggs

    >>> fp_from_eggs(0)
    0.0
    >>> round(fp_from_eggs(2), 4)
    0.2191
    >>> round(fp_from_eggs(13), 4)
    1.4244
    '''
    # convert between units of mass
    fp_from_eggs_daily_grams = daily_eggs * 300
    fp_from_eggs_daily_kg = fp_from_eggs_daily_grams / 1000
    fp_from_eggs_daily_tonnes = kg_to_tonnes(fp_from_eggs_daily_kg)
    # convert from daily to annual
    fp_from_eggs_annual_tonnes = daily_to_annual(fp_from_eggs_daily_tonnes)
    return fp_from_eggs_annual_tonnes

######################################

def fp_of_diet(daily_meat_grams, daily_cheese_grams, daily_milk_litres, daily_eggs):
    '''
    (num, num, num, num) -> flt
    Approximate annual CO2E footprint in metric tonnes, from diet, based on daily consumption of meat in grams, cheese in grams, milk in litres, and eggs.

    Based on https://link.springer.com/article/10.1007%2Fs10584-014-1169-1
    A vegan diet is 2.89 kg CO2E / day in the UK.
    I infer approximately 0.0268 kgCO2E/day per gram of meat eaten.

    This calculation misses forms of dairy that are not milk or cheese, such as ice cream, yogourt, etc.

    From How Bad Are Bananas:
        1 pint of milk (2.7 litres) -> 723 g CO2E 
                ---> 1 litre of milk: 0.2677777 kg of CO2E
        1 kg of hard cheese -> 12 kg CO2E 
                ---> 1 g cheese is 12 g CO2E -> 0.012 kg CO2E
        12 eggs -> 3.6 kg CO2E 
                ---> 0.3 kg CO2E per egg

    >>> round(fp_of_diet(0, 0, 0, 0), 4) # vegan
    1.0556
    >>> round(fp_of_diet(0, 0, 0, 1), 4) # 1 egg
    1.1651
    >>> round(fp_of_diet(0, 0, 1, 0), 4) # 1 L milk
    1.1534
    >>> round(fp_of_diet(0, 0, 1, 1), 4) # egg and milk
    1.2629
    >>> round(fp_of_diet(0, 10, 0, 0), 4) # cheeese
    1.0994
    >>> round(fp_of_diet(0, 293.52, 1, 1), 4) # egg and milk and cheese
    2.5494
    >>> round(fp_of_diet(25, 0, 0, 0), 4) # meat
    1.3003
    >>> round(fp_of_diet(25, 293.52, 1, 1), 4) 
    2.7941
    >>> round(fp_of_diet(126, 293.52, 1, 1), 4)
    3.7827
    '''
    return kg_to_tonnes(daily_to_annual(2.89)) + fp_from_meat(daily_meat_grams) + fp_from_milk(daily_milk_litres) + fp_from_cheese(daily_cheese_grams) + fp_from_eggs(daily_eggs)

#################################################

if __name__ == '__main__':
    doctest.testmod()

