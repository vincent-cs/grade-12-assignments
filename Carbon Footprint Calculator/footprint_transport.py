import doctest
from unit_conversion import *

INCOMPLETE = -1


################################################


def fp_from_driving(annual_km_driven):
    '''
    (num) -> flt
    Approximate CO2E footprint for one year of driving, based on total km driven.
    Result in metric tonnes.
    Source: https://www.forbes.com/2008/04/15/green-carbon-living-forbeslife-cx_ls_0415carbon.html#1f3715d01852
    "D.) Multiply total yearly mileage by .79 (for pounds)"

    >>> fp_from_driving(0)
    0.0
    >>> round(fp_from_driving(100), 4)
    0.0223
    >>> round(fp_from_driving(1234), 4)
    0.2748
    '''
    # first convert to miles (will the US ever switch to metric?)
    annual_miles_driven = km_to_miles(annual_km_driven)
    # convert between units of mass
    fp_from_driving_annual_pounds = annual_miles_driven * 0.79
    fp_from_driving_annual_kg = pound_to_kg(fp_from_driving_annual_pounds)
    fp_from_driving_annual_tonnes = kg_to_tonnes(fp_from_driving_annual_kg)
    return fp_from_driving_annual_tonnes

def fp_from_taxi_uber(weekly_uber_rides):
    '''(num) -> flt
    Estimate in metric tonnes of CO2E the annual footprint from a given
    number of weekly uber/taxi/etc rides.

    Source: https://www.mapc.org/resource-library/the-growing-carbon-footprint-of-ride-hailing-in-massachusetts/
        81 million trips -> 100,000 metric tonnes
        Thus 1 trip -> 0.0012345679012346 metric tonnes
    >>> fp_from_taxi_uber(0)
    0.0
    >>> round(fp_from_taxi_uber(10), 4)
    0.6442
    >>> round(fp_from_taxi_uber(25), 4)
    1.6104
    '''
    # division within is necessary to get  both clean looking code and an accurate result
    fp_from_taxi_uber_weekly_tonnes = weekly_uber_rides * (100000/81000000)
    fp_from_taxi_uber_annual_tonnes = weekly_to_annual(fp_from_taxi_uber_weekly_tonnes)
    return fp_from_taxi_uber_annual_tonnes

def fp_from_transit(weekly_bus_trips, weekly_rail_trips):
    '''
    (num, num) -> flt
    Annual CO2E tonnes from public transit based on number of weekly bus
    rides and weekly rail (metro/commuter train) rides.

    Source: https://en.wikipedia.org/wiki/Transportation_in_Montreal
    The average amount of time people spend commuting with public transit in Montreal, for example to and from work, on a weekday is 87 min. 29.% of public transit riders, ride for more than 2 hours every day. The average amount of time people wait at a stop or station for public transit is 14 min, while 17% of riders wait for over 20 minutes on average every day. The average distance people usually ride in a single trip with public transit is 7.7 km, while 17% travel for over 12 km in a single direction.[28]
    Source: https://en.wikipedia.org/wiki/Société_de_transport_de_Montréal
    As of 2011, the average daily ridership is 2,524,500 passengers: 1,403,700 by bus, 1,111,700 by rapid transit and 9,200 by paratransit service.

    Source: How Bad Are Bananas
        A mile by bus: 150g CO2E
        A mile by subway train: 160g CO2E for London Underground

    >>> fp_from_transit(0, 0)
    0.0
    >>> round(fp_from_transit(1, 0), 4)
    0.0374
    >>> round(fp_from_transit(0, 1), 4)
    0.0399
    >>> round(fp_from_transit(10, 2), 4)
    0.4544
    '''
    # first find bus annual tonnes CO2E, with conversions between units
    weekly_bus_trips_length_km = weekly_bus_trips * 7.7
    weekly_bus_trips_length_miles = km_to_miles(weekly_bus_trips_length_km)
    # convert to annual
    annual_bus_trips_length_miles = weekly_to_annual(weekly_bus_trips_length_miles)
    # now we can find the fp
    fp_from_bus_trips_annual_grams = annual_bus_trips_length_miles * 150
    fp_from_bus_trips_annual_kg = fp_from_bus_trips_annual_grams/1000
    fp_from_bus_trips_annual_tonnes = kg_to_tonnes(fp_from_bus_trips_annual_kg)

    # now rail annual tonnes CO2E, with conversion between units
    weekly_rail_trips_length_km = weekly_rail_trips * 7.7
    weekly_rail_trips_length_miles = km_to_miles(weekly_rail_trips_length_km)
    # convert to annual
    annual_rail_trips_length_miles = weekly_to_annual(weekly_rail_trips_length_miles)
    # now we can find the fp
    fp_from_rail_trips_annual_grams = annual_rail_trips_length_miles * 160
    fp_from_rail_trips_annual_kg = fp_from_rail_trips_annual_grams/1000
    fp_from_rail_trips_annual_tonnes = kg_to_tonnes(fp_from_rail_trips_annual_kg)

    # return the sum of both
    return fp_from_rail_trips_annual_tonnes + fp_from_bus_trips_annual_tonnes

def fp_of_transportation(weekly_bus_rides, weekly_rail_rides, weekly_uber_rides, weekly_km_driven):
    '''(num, num, num, num) -> flt
    Estimate in tonnes of CO2E the footprint of weekly transportation given
    specified annual footprint in tonnes of CO2E from diet.

    >>> fp_of_transportation(0, 0, 0, 0)
    0.0
    >>> round(fp_of_transportation(2, 2, 1, 10), 4)
    0.3354
    >>> round(fp_of_transportation(1, 2, 3, 4), 4)
    0.3571
    '''
    return fp_from_driving(weekly_to_annual(weekly_km_driven)) + fp_from_taxi_uber(weekly_uber_rides) + fp_from_transit(weekly_bus_rides, weekly_rail_rides)


#################################################

# You might want to put helper functions here :)

def fp_from_long_flight(annual_long_flights):
    '''
    (num) -> flt
    Estimate the metric tonnes of CO2E based on annual number of long flights taken

    >>> fp_from_long_flight(0)
    0.0
    >>> round(fp_from_long_flight(2), 4)
    3.9916
    >>> round(fp_from_long_flight(13), 4)
    25.9455
    '''
    # conversion between units of mass
    fp_from_long_flights_pounds = annual_long_flights * 4400
    fp_from_long_fligts_kg = pound_to_kg(fp_from_long_flights_pounds)
    fp_from_long_flights_tonnes = kg_to_tonnes(fp_from_long_fligts_kg)
    return fp_from_long_flights_tonnes

def fp_from_short_flight(annual_short_flights):
    '''
    (num) -> flt
    Estimate the metric tonnes of CO2E based on annual number of short flights taken

    >>> fp_from_short_flight(0)
    0.0
    >>> round(fp_from_short_flight(2), 4)
    0.9979
    >>> round(fp_from_short_flight(13), 4)
    6.4864
    '''
    # conversions between units of mass
    fp_from_short_flights_pounds = annual_short_flights * 1100
    fp_from_short_fligts_kg = pound_to_kg(fp_from_short_flights_pounds)
    fp_from_short_flights_tonnes = kg_to_tonnes(fp_from_short_fligts_kg)
    return fp_from_short_flights_tonnes

def fp_from_train(annual_train):
    '''
    (num) -> flt
    Estimate the metric tonnes of CO2E based on annual number of train ride taken

    >>> fp_from_train(0)
    0.0
    >>> round(fp_from_train(2), 4)
    0.0689
    >>> round(fp_from_train(13), 4)
    0.4479
    '''
    fp_from_train_ride_kg = annual_train * 34.45
    fp_from_train_ride_tonnes = kg_to_tonnes(fp_from_train_ride_kg)
    return fp_from_train_ride_tonnes

def fp_from_coach(annual_coach):
    '''
    (num) -> flt
    Estimate the metric tonnes of CO2E based on annual number of bus rides taken
    These tests are not testing bus coach rides, but repeating the same tests as the previous function. Thus extra care
    should be taken with this function, unless proper tests are added.
    >>> fp_from_train(0)
    0.0
    >>> round(fp_from_train(2), 4)
    0.0689
    >>> round(fp_from_train(13), 4)
    0.4479
    '''
    # convert between units of mass
    fp_from_coach_ride_kg = annual_coach * 33
    fp_from_coach_ride_tonnes = kg_to_tonnes(fp_from_coach_ride_kg)
    return fp_from_coach_ride_tonnes

def fp_from_hotel(annual_hotels):
    '''
    (num) -> flt
    Estimate the metric tonnes of CO2E based on annual number spent on a hotel stay

    >>> fp_from_hotel(0)
    0.0
    >>> round(fp_from_hotel(2), 4)
    0.0005
    >>> round(fp_from_hotel(13), 4)
    0.0035
    '''
    # convert between units of mass
    fp_from_hotel_grams = annual_hotels * 270
    fp_from_hotel_kg = fp_from_hotel_grams/1000
    fp_from_hotel_tonnes = kg_to_tonnes(fp_from_hotel_kg)
    return fp_from_hotel_tonnes

#################################################

def fp_of_travel(annual_long_flights, annual_short_flights, annual_train, annual_coach, annual_hotels):
    '''(num, num, num, num, num) -> float
    Approximate CO2E footprint in metric tonnes for annual travel, based on number of long flights (>4 h), short flights (<4), intercity train rides, intercity coach bus rides, and spending at hotels.

    Source for flights: https://www.forbes.com/2008/04/15/green-carbon-living-forbeslife-cx_ls_0415carbon.html#1f3715d01852 --- in lbs
    "E.) Multiply the number of flights--4 hours or less--by 1,100 lbs
    F.) Multiply the number of flights--4 hours or more--by 4,400 libs"

    Source for trains: https://media.viarail.ca/sites/default/files/publications/SustainableMobilityReport2016_EN_FINAL.pdf
    137,007 tCO2E from all of Via Rail, 3974000 riders
        -> 34.45 kg CO2E

    Source for buses: How Bad Are Bananas
        66kg CO2E for ROUND TRIP coach bus ride from NYC to Niagara Falls
        I'm going to just assume that's an average length trip, because better data not readily avialible.

    Source for hotels: How Bad Are Bananas
        270 g CO2E for every dollar spent

    >>> fp_of_travel(0, 0, 0, 0, 0)
    0.0
    >>> round(fp_of_travel(0, 1, 0, 0, 0), 4) # short flight
    0.499
    >>> round(fp_of_travel(1, 0, 0, 0, 0), 4) # long flight
    1.9958
    >>> round(fp_of_travel(2, 2, 0, 0, 0), 4) # some flights
    4.9895
    >>> round(fp_of_travel(0, 0, 1, 0, 0), 4) # train
    0.0345
    >>> round(fp_of_travel(0, 0, 0, 1, 0), 4) # bus
    0.033
    >>> round(fp_of_travel(0, 0, 0, 0, 100), 4) # hotel
    0.027
    >>> round(fp_of_travel(6, 4, 24, 2, 2000), 4) # together
    15.4034
    >>> round(fp_of_travel(1, 2, 3, 4, 5), 4) # together
    3.2304
    '''
    return fp_from_long_flight(annual_long_flights) + fp_from_short_flight(annual_short_flights) + fp_from_train(annual_train) + fp_from_coach(annual_coach) + fp_from_hotel(annual_hotels)

#################################################

if __name__ == '__main__':
    doctest.testmod()
